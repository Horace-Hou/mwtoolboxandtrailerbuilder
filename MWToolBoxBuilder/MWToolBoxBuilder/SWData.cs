﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MWToolBoxBuilder
{
    public static class SWData
    {
    }

    [Serializable]
    public class SWProject
    {

    }

    [Serializable]
    public class SWTemplate
    {
        public string ID;
    }

    [Serializable]
    public class SWAsm
    {
        public string name;
        public string localPath;
        public List<SubSWAsm> subAsms = new List<SubSWAsm>();
        public List<SWPart> parts = new List<SWPart>();
    }

    [Serializable]
    public class SubSWAsm
    {
        public string name;
        public string localPath;
        public List<SWPart> parts = new List<SWPart>();
    }
    
    [Serializable]
    public class SWPart
    {
        public string name;
        public string localPath;
        public Dictionary<string, double> dimensions = new Dictionary<string, double>();
    }
}
